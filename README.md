## DevLocker Frontend

[![Circle CI](https://circleci.com/gh/devlocker/frontend/tree/master.svg?style=svg)](https://circleci.com/gh/devlocker/frontend/tree/master)

## Installation

```
npm install
```

## Running Dev Server

```
npm run dev
```

### Testing

```
npm test
```
