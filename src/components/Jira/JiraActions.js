var JiraActions = Reflux.createActions([
  "load",
  "fetch"
]);

module.exports = JiraActions;
