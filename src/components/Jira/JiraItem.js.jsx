var JiraActions = require("./JiraActions");

var JiraItem = React.createClass({
  getInitialState() {
    return {
      description: this.props.model.get("description"),
      summary: this.props.model.get("summary")
    };
  },

  render() {
    return(
      <li key={this.props.model.id}>
        <p>{this.state.description}</p>
        <p>{this.state.summary}</p>
      </li>
    );
  }
});

module.exports = JiraItem;
