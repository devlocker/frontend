var JiraActions = require("./JiraActions");
var JiraStore = require("./JiraStore");
var JiraItem = require("./JiraItem.js.jsx");
var SetIntervalMixin = require("../../mixins/SetIntervalMixin");

var JiraList = React.createClass({
  mixins: [Reflux.connect(JiraStore, "list"), SetIntervalMixin],

  getInitialState() {
    return { list: [] }
  },

  componentDidMount() {
    JiraActions.load();
    // this.setInterval(TodoActions.fetch, 5000);
  },

  render() {
    return(
      <article className="widget">
        <header>
          <h1>Jira</h1>
        </header>
        <article className="jira-list">
          <section>
            <ul>
              {this.state.list.map(item => {
                  return(
                    <JiraItem
                      key={item.id}
                      model={item}
                    />
                  )
              })
            }
            </ul>
          </section>
        </article>
      </article>
    );
  }
});

module.exports = JiraList;
