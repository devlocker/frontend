var JiraActions = require("./JiraActions");

var JiraStore = Reflux.createStore({
  listenables: [JiraActions],

  load() {
    var JiraItemsCollection = Backbone.Collection.extend({
      url: window.location.pathname + "/jira/items"
    });

    var collection = new JiraItemsCollection;

    collection.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  fetch() {
    this.list.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  updateList() {
    this.trigger(this.list);
  }
});

module.exports = JiraStore;
