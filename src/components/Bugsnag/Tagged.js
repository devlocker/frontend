import React, { Component, PropTypes } from 'react';
import Error from "./Error"

class Tagged extends Component {
  render() {
    return(
      <section>
        <h2>Tagged Errors</h2>
        {this.props.errors.map( (error) =>
          <Error
            class={error.class}
            message={error.last_message}
            occurances={error.occurances}
            usersAffected={error.usersAffected}
            url={error.url}
          />
        )}
      </section>
    );
  }
}

export default Tagged;
