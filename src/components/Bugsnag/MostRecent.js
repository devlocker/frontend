import React, { Component, PropTypes } from 'react';
import Error from './Error'

class MostRecent extends Component {
  render() {
    return(
      <section>
        <section className='list-header'>
          <h3>Most Recent Errors</h3>
        </section>
        {this.props.errors.map(error =>
          <Error
            class={error.get('class')}
            message={error.get('last_message')}
            occurrences={error.get('occurrences')}
            usersAffected={error.get('users_affected')}
            lastReceived={error.get('last_received')}
            url={error.get('html_url')}
          />
        )}
      </section>
    );
  }
}

export default MostRecent;
