import React, { Component, PropTypes } from 'react';

class Error extends Component {
  render() {
    return(
        <section>
          <h3><a href={this.props.url}>{this.props.class}</a></h3>
          <div>{this.props.message}</div>
          <div>{this.props.occurrences} occurrences</div>
          <div>{this.props.usersAffected} users affected</div>
          <div>last received {this.props.lastReceived}</div>
        </section>
    )
  }
}

export default Error;
