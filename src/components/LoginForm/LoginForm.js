import React, { Component, PropTypes } from 'react';

export default class LoginForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);
    this.styles = require('./LoginForm.scss');
    this.state = {step: 1};
  }

  _emailKeyDown(e) {
    if(e.keyCode == 13) {
      e.preventDefault();
      let line = React.findDOMNode(this.refs.email)
      line.readOnly = true

      this.setState({ step: 2, email: line.value });
    }
  }

  _passwordKeyDown(e) {
    if(e.keyCode == 13) {
      e.preventDefault();
      let line = React.findDOMNode(this.refs.password)
      line.readOnly = true

      this.setState({ password: line.value });
      this.props.handleSubmit(this);
    }
  }

  emailField() {
    return (
      <div className={this.styles.terminalGroup}>
        <span>[enter email] $</span>
        <input
          className={this.styles.terminalField}
          type='text'
          ref='email'
          onKeyDown={::this._emailKeyDown}
          autoFocus={true}
        />
      </div>
    )
  }

  passwordField() {
    return (
      <div className={this.styles.terminalGroup}>
        <span>[enter password] $</span>
        <input
          className={this.styles.terminalField}
          type='password'
          ref='password'
          onKeyDown={::this._passwordKeyDown}
          autoFocus={true}
        />
      </div>
    );
  }

  render() {
    return (
      <form>
        { this.emailField() }
        { this.state.step > 1 && this.passwordField() }
      </form>
    );
  }
}
