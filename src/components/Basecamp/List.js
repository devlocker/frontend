import React, { Component, PropTypes } from 'react';
import Todo from './Todo';

export default class List extends Component {
  componentDidMount() {
    this.props.loadTodos(this.props.id);
  }

  render() {
    return(
      <li>
        <a href={this.props.appURL} target="_blank">{this.props.name}</a>
        {this.props.todos.map(todo => {
          return(
            <Todo
              key={todo.get('id')}
              id={todo.get('id')}
              content={todo.get('content')}
              appURL={todo.get('app_url')}
              completed={todo.get('completed')}
              completeTodo={(todoId) => 
                { this.props.completeTodo(this.props.id, todoId) }
              }
            />
          )})
        }
      </li>
    );
  }
}

