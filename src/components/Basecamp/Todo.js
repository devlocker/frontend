import React, { Component, PropTypes } from 'react';

export default class Todo extends Component {
  _toggled(e) {
    this.props.completeTodo(this.props.id)
  }

  formattedContent(content) {
    return(
      content.length > 45 ? content.slice(0, 46) + "..." : content
    )
  }

  render() {
    return(
      <div className="toggle">
      &nbsp;&nbsp;
        <input
          type="checkbox"
          onChange={this._toggled.bind(this)}
          checked={this.props.completed}
        />
      &nbsp;<a href={this.props.appURL} target="_blank">{this.formattedContent(this.props.content)}</a>
      </div>
    );
  }
}
