import React, { Component, PropTypes } from 'react';
import { Map } from 'immutable';
import List from './List';

export default class MainSection extends Component {
  componentDidMount() {
    this.props.loadLists();
  }

  render() {
    return(
      <article className="widget">
        <header>
          <h1>Basecamp</h1>
        </header>
        <article className="basecamp-list">
          <section>
            {this.props.lists.map(list => {
              return(
                <List
                  key={list.get('id')}
                  id={list.get('id')}
                  name={list.get('name')}
                  appURL={list.get('app_url')}
                  todos={list.get('todos') || Map()}
                  loadTodos={this.props.loadTodos}
                  completeTodo={this.props.completeTodo}
                />
              )})
            }
          </section>
        </article>

        <footer>
          <a href="#">Refresh</a> / <a href="#">Settings</a>
        </footer>
      </article>
    );
  }
}
