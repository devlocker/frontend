import React, { Component } from 'react';

export default class EmptyAddon extends Component {
  render() {
    return(
      <div>
        <p>
          Want some apps? Go to the <a href={`${window.location}/addons`}>App Store</a> and add some!
        </p>
      </div>
    );
  }
}
