var SignUpTerminalActions = require("./SignUpTerminalActions");

var SignUpTerminalStore = Reflux.createStore({
  listenables: [SignUpTerminalActions],

  init() {
    this.step = 1;
    this.resetKey = Math.random();
    this.userInputs = {};
  },

  getInitialState() {
    return({
      step: this.step,
      resetKey: this.resetKey,
      emailValid: true,
      userCreateFailed: false
    });
  },

  submitField(text) {
    switch(this.step) {
    case 1:
      $.ajax({
        url: "/users/validate_email",
        data: { email: text },
        success: (data) => {
          if(data.exists === false) {
            this.emailValid = true
            this.step = this.step + 1;
            this.trigger({ step: this.step, emailValid: true });
            this.userInputs.email = text
          } else {
            this.emailValid = false
            this.trigger({ step: this.step, emailValid: false });
          }
        }
      });
      break

    // No break to allow default to run for rest of the cases
    case 2:
      this.userInputs.first_name = text;
    case 3:
      this.userInputs.last_name = text;
    case 4:
      this.userInputs.password = text;
    case 5:
      this.userInputs.password_confirmation = text;

    default:
      this.step = this.step + 1;
      this.trigger({ step: this.step });
    }
  },

  submitForm() {
    $.ajax({
      url: "/signup.json",
      method: "POST",
      data: { user: this.userInputs },
      success: () => {
        this.step = this.step + 1;
        this.trigger({ step: this.step });
      },
      error: () => {
        this.step = this.step + 1;
        this.userCreateFailed = true;
        this.trigger({ userCreateFailed: true, step: this.step });
      }
    });
  },

  reset() {
    this.step = 1;
    this.userCreateFailed = false;
    this.emailValid = true;
    this.resetKey = Math.random();

    this.trigger({
      step: this.step,
      resetKey: this.resetKey,
      emailValid: this.emailValid,
      userCreateFailed: this.userCreateFailed
    });
  }
});

module.exports = SignUpTerminalStore;
