var SignUpTerminalActions = Reflux.createActions([
  "submitField",
  "validateEmail",
  "submitForm",
  "reset"
]);

module.exports = SignUpTerminalActions;
