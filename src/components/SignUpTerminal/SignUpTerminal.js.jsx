var TimerMixin = require('react-timer-mixin');
var SignUpTerminalActions = require("./SignUpTerminalActions")
var SignUpTerminalStore = require("./SignUpTerminalStore");

var SignUpTerminal = React.createClass({
  mixins: [
    Reflux.connect(SignUpTerminalStore)
  ],

  _onCtrlC() {
    this.setState({ step: 1, key: Math.random() });
    this.forceUpdate();
  },

  render() {
    return(
      <div className="sign-up-terminal">
        <p>* Booting...</p>
        <p>* DevLocker Sign Up</p>
        <p>* Already have an account? <a href="/signin">Sign In</a></p>
        <br/>
        <br/>
        <p>* Please enter your information</p>
        <p>* Press Ctrl + C to restart</p>
        <br/>
        <br/>

        { this.state.step >= 1 &&
          <SignUpTerminalEmailField
            id="email"
            key = { this.state.resetKey + 1 }
            prompt="[enter email] $"
          />
        }
        { this.state.emailValid === false && <SignUpTerminalInvalidEmail /> }

        { this.state.step >= 2 &&
          <SignUpTerminalEmailField
            key={ this.state.resetKey + 2 }
            id="first_name"
            prompt="[enter first name] $"
          />
        }
        { this.state.step >= 3 &&
          <SignUpTerminalEmailField
            key={ this.state.resetKey  + 3 }
            id="last_name"
            prompt="[enter last name] $"
          />
        }
        { this.state.step >= 4 &&
          <SignUpTerminalEmailField
            key={ this.state.resetKey  + 4 }
            id="password"
            prompt="[enter password] $"
            type="password"
          />
        }
        { this.state.step >= 5 &&
          <SignUpTerminalEmailField
            key={ this.state.resetKey  + 5 }
            id="password_confirmation"
            prompt="[retype password] $"
            type="password"
          />
        }

        { this.state.step == 6 && <p>Creating your account</p> && SignUpTerminalActions.submitForm() }
        { this.state.step >= 7 && this.state.userCreateFailed === false && <AsciiHello /> }
        { this.state.step >= 7 &&
          this.state.userCreateFailed === true &&
          <p className="error">Creation Failed. Press CTRL+C to restart and try again </p>
        }
      </div>
    );
  }
});

var SignUpTerminalEmailField = React.createClass({
  getInitialState() {
    return { readOnly: false }
  },

  componentDidMount() {
    this.refs.terminalField.getDOMNode().focus();
  },

  handleKeyDown(event) {
    if (event.keyCode == 13 ) {
      event.preventDefault();
      this.setState({ readOnly: true })
      SignUpTerminalActions.submitField(this.refs.terminalField.getDOMNode().value);
    }
    else if (event.ctrlKey && event.keyCode == 67) {
      event.preventDefault();
      SignUpTerminalActions.reset();
    }
  },

  render() {
    return(
      <div className="terminal-group">
        {this.props.prompt}
        <input
          id={this.props.id}
          key={this.props.key}
          tabIndex="1"
          ref="terminalField"
          className="terminal-field"
          contentEditable="true"
          readOnly={ this.state.readOnly }
          type={this.props.type}
          onKeyDown={this.handleKeyDown}>
        </input>
      </div>
    );
  }
});

var SignUpTerminalInvalidEmail = React.createClass({
  render() {
    return(
      <div>
        <p>Verifying Email...</p>
        <p className="error">This email address has already been taken.</p>
      </div>
    );
  }
});

var AsciiHello = React.createClass({
  mixins: [TimerMixin],

  componentDidMount() {
    this.setTimeout(
      () => { window.location.href = "~"; },
      1000
    );
  },

  render() {
    return(
      <div>
        <pre>{' __     __     ______     __         ______     ______     __    __     ______                   '}</pre>
        <pre>{'/\\ \\  _ \\ \\   /\\  ___\\   /\\ \\       /\\  ___\\   /\\  __ \\   /\\ "-./  \\   /\\  ___\\                  '}</pre>
        <pre>{'\\ \\ \\/ ".\\ \\  \\ \\  __\\   \\ \\ \\____  \\ \\ \\____  \\ \\ \\/\\ \\  \\ \\ \\-./\\ \\  \\ \\  __\\                  '}</pre>
        <pre>{' \\ \\__/".~\\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\_____\\  \\ \\_____\\  \\ \\_\\ \\ \\_\\  \\ \\_____\\                '}</pre>
        <pre>{'  \\/_/   \\/_/   \\/_____/   \\/_____/   \\/_____/   \\/_____/   \\/_/  \\/_/   \\/_____/                '}</pre>
        <pre>{'                                                                                                 '}</pre>
        <pre>{' ______   ______                                                                                 '}</pre>
        <pre>{'/\\__  _\\ /\\  __ \\                                                                                '}</pre>
        <pre>{'\\/_/\\ \\/ \\ \\ \\/\\ \\                                                                               '}</pre>
        <pre>{'   \\ \\_\\  \\ \\_____\\                                                                              '}</pre>
        <pre>{'    \\/_/   \\/_____/                                                                              '}</pre>
        <pre>{'                                                                                                 '}</pre>
        <pre>{' _____     ______     __   __   __         ______     ______     __  __     ______     ______    '}</pre>
        <pre>{'/\\  __-.  /\\  ___\\   /\\ \\ / /  /\\ \\       /\\  __ \\   /\\  ___\\   /\\ \\/ /    /\\  ___\\   /\\  == \\   '}</pre>
        <pre>{'\\ \\ \\/\\ \\ \\ \\  __\\   \\ \\ \\"/   \\ \\ \\____  \\ \\ \\/\\ \\  \\ \\ \\____  \\ \\  _"-.  \\ \\  __\\   \\ \\  __<   '}</pre>
        <pre>{' \\ \\____-  \\ \\_____\\  \\ \\__|    \\ \\_____\\  \\ \\_____\\  \\ \\_____\\  \\ \\_\\ \\_\\  \\ \\_____\\  \\ \\_\\ \\_\\ '}</pre>
        <pre>{'  \\/____/   \\/_____/   \\/_/      \\/_____/   \\/_____/   \\/_____/   \\/_/\\/_/   \\/_____/   \\/_/ /_/ '}</pre>
        <pre>{'                                                                                               '}</pre>
     </div>
    )
  }
});

module.exports = SignUpTerminal;
