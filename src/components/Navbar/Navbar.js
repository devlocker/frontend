import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class Navbar extends Component {
  render() {
    const { user, handleLogout } = this.props;
    const styles = require('./Navbar.scss');
    return (
      <nav className={styles.navbar}>
        <div className={styles.branding}>
          <a className={styles.important} href='#'>DevLocker</a>
          <span>/</span>
          <a className={styles.sub} href='#'>DevLocker</a>
        </div>
        <div className={styles.arrowRight}></div>

        <div className={styles.right}>
          <span className={styles.info}>{new Date}</span>
          {!user && <Link to="/login">Login</Link>}
          {user && <a className={styles.appStore} href='#'>App Store</a>}
          {user && <a href="/logout" onClick={handleLogout}>Logout</a>}
          {user && <p className={styles.loggedInMessage + ' navbar-text'}>Logged in as <strong>{user.name}</strong>.</p>}
        </div>
      </nav>
    );
  }
}
