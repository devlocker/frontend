var ColorPicker = React.createClass({
  selectColor(color) {
    this.props.actions.changeColor(color);
  },

  render() {
    return(
      <div className="color-picker">
        <a href="#" onClick={this.selectColor.bind(this, "color-1")}>
          <div className="color-option color-1"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-2")}>
          <div className="color-option color-2"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-3")}>
          <div className="color-option color-3"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-4")}>
          <div className="color-option color-4"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-5")}>
          <div className="color-option color-5"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-6")}>
          <div className="color-option color-6"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-7")}>
          <div className="color-option color-7"></div>
        </a>
        <a href="#" onClick={this.selectColor.bind(this, "color-8")}>
          <div className="color-option color-8"></div>
        </a>
      </div>
    )
  }
});

module.exports = ColorPicker;
