var AppActions = require("./AppActions");
var ColorPicker = require("../Utilities/ColorPicker.js.jsx");

var Settings = React.createClass({
  changeToListView() {
    AppActions.changeView("List");
  },

  render() {
    return(
      <div className="widget">
        <header>
          <h1>Todo List Settings</h1>
        </header>
        <section>
          <p>Color:</p>
          <ColorPicker actions={AppActions} />
        </section>
        <footer>
          <a href="#" onClick={this.changeToListView}>Save</a>
        </footer>
      </div>
    )
  }
});

module.exports = Settings;
