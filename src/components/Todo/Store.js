var Actions = require("./Actions");

var Store = Reflux.createStore({
  listenables: [Actions],

  load() {
    var TodoItemsCollection = Backbone.Collection.extend({
      url: window.location.pathname + "/todo/items"
    });

    var collection = new TodoItemsCollection;

    collection.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  fetch() {
    this.list.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  addItem(text) {
    this.list.create({ content: text, completed: false });
    this.updateList();
  },

  toggleItem(model) {
    model.save({ completed: !model.get("completed") });
    this.updateList();
  },

  editItem(model, content) {
    model.save({ content: content });
    this.updateList();
  },

  deleteItem(model) {
    model.destroy();
    this.updateList();
  },

  updateList() {
    this.trigger(this.list);
  },

  changeColor(color) {
    this.trigger({ color: color });
  }
});

module.exports = Store;
