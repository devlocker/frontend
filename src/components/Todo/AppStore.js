var AppActions = require("./AppActions");

var AppStore = Reflux.createStore({
  listenables: [AppActions],

  setDefaults(options) {
    this.options = options;
  },

  changeColor(color) {
    $.ajax({
      url: (window.location.pathname + "/addons/" + this.options.addonId + "/update_color"),
      method: "PATCH",
      data: { addon: { metadata: { color: color } } }
    });
    this.trigger({ color: color });
  },

  changeView(view) {
    this.trigger({ view: view });
  }
});

module.exports = AppStore;
