var Actions = require("./Actions");

var Item = React.createClass({
  getInitialState() {
    return { editing: false, content: this.props.model.get("content") };
  },

  toggled(event) {
    Actions.toggleItem(this.props.model);
  },

  _completed() {
    return this.props.model.get("completed")
  },

  deleteItem() {
    Actions.deleteItem(this.props.model);
  },

  editItem() {
    this.setState({ editing: true });
  },

  handleBlur(event) {
    this.setState({ editing: false });
    Actions.editItem(this.props.model, this.state.content);
  },

  handleChange(event) {
    this.setState({ content: event.target.value })
  },

  handleKeyDown(event) {
    var ENTER_KEY = 13;
    var ESC_KEY = 27;

    if (event.keyCode == ENTER_KEY) {
      Actions.editItem(this.props.model, event.target.value);
      this.setState({ editing: false });
    } else if(event.keyCode == ESC_KEY) {
      this.setState({ editing: false, content: this.props.model.get("content") });
    }
  },

  renderTitle() {
    return(
      <div onDoubleClick={this.editItem} className="content">
        {this.state.content}
      </div>
    );
  },

  renderInlineEdit() {
    return(
      <input
        ref="editField"
        value={this.state.content}
        autoFocus={true}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        className="edit-field"
      />
    );
  },

  render() {
    return(
      <li key={this.props.model.id} className={this._completed() ? "completed" : ""}>
        <div className="toggle">
          <input
            type="checkbox"
            onChange={this.toggled}
            checked={this.props.model.get("completed")}
          />
        </div>
        {this.state.editing ? this.renderInlineEdit() : this.renderTitle()}
        <button onClick={this.deleteItem} className="destroy" type="button">
        X
        </button>
      </li>
    );
  }
});

module.exports = Item;
