var Actions = Reflux.createActions([
  "load",
  "fetch",
  "toggleItem",
  "addItem",
  "editItem",
  "deleteItem"
]);

module.exports = Actions;
