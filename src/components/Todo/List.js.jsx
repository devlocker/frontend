var Actions = require("./Actions");
var AppActions = require("./AppActions");
var Store = require("./Store");
var Item = require("./Item.js.jsx");
var SetIntervalMixin = require("../../mixins/SetIntervalMixin");

var List = React.createClass({
  mixins: [Reflux.connect(Store, "list"), SetIntervalMixin],

  getInitialState() {
    return { list: [] }
  },

  componentDidMount() {
    Actions.load();
    // this.setInterval(Actions.fetch, 5000);
  },

  handleNewTodoKeyDown(event) {
    var ENTER_KEY = 13;

    if (event.which !== ENTER_KEY) {
      return;
    }

    event.preventDefault();

    var text = this.refs.newField.getDOMNode().value.trim();

    if (text) {
      Actions.addItem(text);
      this.refs.newField.getDOMNode().value = '';
    }
  },

  changeToSettingsView() {
    AppActions.changeView("Settings");
  },

  render() {
    return(
      <div className={`widget ${this.props.color}`}>
        <header>
          <h1>Todo List</h1>
        </header>
        <article className="todo-list">
          <section>
            <ul>
              {this.state.list.map(item => {
                  return(
                    <Item
                      key={item.id}
                      model={item}
                    />
                  )
              })
            }
            </ul>
          </section>
          <section className="todo-list-add">
            <input
              ref="newField"
              id="new-todo"
              className="input-form"
              placeholder="+ Add Another"
              autoFocus={true}
              onKeyDown={this.handleNewTodoKeyDown}
            />
          </section>
        </article>
        <footer>
          <a href="#">Refresh</a> / <a href="#" onClick={this.changeToSettingsView}>Settings</a>
        </footer>
      </div>
    );
  }
});

module.exports = List;
