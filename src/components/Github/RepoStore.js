var RepoActions = require("./RepoActions");

var RepoStore = Reflux.createStore({
  listenables: [RepoActions],

  loadRepos() {
    var url = `${window.location.pathname}/github/repos`;

    var ReposCollection = Backbone.Collection.extend({
      url: url
    });

    var collection = new ReposCollection();

    collection.fetch({
      success: (collection) => {
        this.repos = collection;
        this.updateRepos();
      }
    });
  },

  toggleRepo(model) {
    model.save({ connect_repo: !model.get("connect_repo") });
    this.updateRepos();
  },

  updateRepos() {
    this.trigger(this.repos);
  }
});

module.exports = RepoStore;
