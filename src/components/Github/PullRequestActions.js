var PullRequestActions = Reflux.createActions([
  "loadPullRequests",
  "refresh"
]);

module.exports = PullRequestActions;
