var PullRequestView = require("./PullRequestView.js.jsx");
var SettingsView = require("./SettingsView.js.jsx");
var AppStore = require("./AppStore");
var AppActions = require("./AppActions");

var App = React.createClass({
  mixins: [Reflux.connect(AppStore)],

  getDefaultProps: function() {
    return {
      color: "color-1"
    }
  },

  getInitialState() {
    return({ view: "List", color: this.props.color })
  },

  componentWillMount() {
    AppActions.setDefaults({ addonId: this.props.addonId })
  },

  switchToListView(event) {
    event.preventDefault();
    this.setState({ view: "List" });
  },

  view() {
    switch(this.state.view) {
      case "List": return <PullRequestView color={this.state.color} />
      case "Settings": return <SettingsView />
    }
  },

  render() {
    return(this.view())
  },
});

module.exports = App;
