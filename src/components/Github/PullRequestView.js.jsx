var PullRequestStore = require("./PullRequestStore");
var PullRequestActions = require("./PullRequestActions");
var AppActions = require("./AppActions");

var PullRequestView = React.createClass({
  mixins: [
    Reflux.connect(PullRequestStore, "pullRequests"),
  ],

  getInitialState() {
    return {
      pullRequests: []
    }
  },

  componentDidMount() {
    PullRequestActions.loadPullRequests();
  },

  _pullRequests() {
    var PRs = this.state.pullRequests.map((pullRequest, index) => {
      return(
        <PullRequest
          title={pullRequest.get("title")}
          repo={pullRequest.get("repo")}
          htmlUrl={pullRequest.get("html_url")}
          url={pullRequest.url}
          key={index}
        />
      )
    });

    return PRs;
  },

  _changeToSettingsView() {
    AppActions.changeView("Settings");
  },

  render() {
    return(
      <div className={`widget ${this.props.color}`}>
        <header>
          <h1>Github</h1>
        </header>

        <article className="basic-list">
          <section className="list-header">
            <p>Created Pull Requests</p>
          </section>
          <section>
            {this._pullRequests()}
          </section>
        </article>

        <footer>
          <div className="widget-controls">
            <a onClick={PullRequestActions.refresh} href="#">Refresh</a>
            <span> / </span>
            <a onClick={this._changeToSettingsView} href="#">Settings</a>
          </div>
        </footer>
      </div>
    );
  },
});

var PullRequest = React.createClass({
  render() {
    return(
      <div className="list-event">
        <span>
          <a href={this.props.htmlUrl} target="_blank">
            {this.props.title}
          </a>
          </span>
          <p className="list-content">
            <span>{this.props.repo}</span>
          </p>
      </div>
    )
  }
});

module.exports = PullRequestView;
