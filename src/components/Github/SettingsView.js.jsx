var RepoActions = require("./RepoActions");
var RepoStore = require("./RepoStore");
var AppActions = require("./AppActions");
var ColorPicker = require("../Utilities/ColorPicker.js.jsx");

var SettingsView = React.createClass({
  mixins: [
    Reflux.connect(RepoStore, "repos"),
  ],

  getInitialState() {
    return {
      repos: []
    }
  },

  componentDidMount() {
    RepoActions.loadRepos();
  },

  _repos() {
    var repos = this.state.repos.map((repo, index) => {
      return(
        <Repo
          model={repo}
          key={index}
        />
      )
    })

    return repos;
  },

  changeToListView() {
    AppActions.changeView("List");
  },

  render() {
    return(
      <article className="widget">
        <header>
          <h1>Github - Settings</h1>
        </header>
        <section>
          <p>Color:</p>
          <ColorPicker actions={AppActions} />
        </section>
        <section>
          <p>Linked Repositories:</p>
          {this._repos()}
        </section>
        <section>
          <div>
            <button onClick={this.loadRepos} type="button">
              Sync Repos
            </button>
          </div>
        </section>
        <footer>
          <div className="widget-controls">
            <a href="#" onClick={this.changeToListView}>Save</a>
          </div>
        </footer>
      </article>
    );
  }
});

var Repo = React.createClass({
  toggled() {
    RepoActions.toggleRepo(this.props.model);
  },

  render() {
    return(
      <div>
        <input
          type="checkbox"
          name="repo"
          onChange={this.toggled}
          checked={this.props.model.get("connect_repo")}
        />
        {this.props.model.get("name")}
     </div>
    )
  }
});

module.exports = SettingsView;
