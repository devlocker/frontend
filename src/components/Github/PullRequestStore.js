var PullRequestActions = require("./PullRequestActions");

var PullRequestStore = Reflux.createStore({
  listenables: [PullRequestActions],

  loadPullRequests() {
    var PullRequestCollection = Backbone.Collection.extend({
      url: `${window.location.pathname}/github/pull_requests`
    });

    var collection = new PullRequestCollection();

    collection.fetch({
      success: (collection) => {
        this.pullRequests = collection;
        this.updatePullRequests();
      }
    });
  },

  refresh() {
    this.pullRequests.create({});
    this.loadPullRequests();
  },

  updatePullRequests() {
    this.trigger(this.pullRequests);
  },
});

module.exports = PullRequestStore;
