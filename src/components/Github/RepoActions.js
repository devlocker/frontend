var RepoActions = Reflux.createActions([
  "loadRepos",
  "toggleRepo"
]);

module.exports = RepoActions;
