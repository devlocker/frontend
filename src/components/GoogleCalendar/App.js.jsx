var List = require("./List.js.jsx");
var Settings = require("./Settings.js.jsx");
var AppStore = require("./AppStore");
var AppActions = require("./AppActions");

var App = React.createClass({
  mixins: [Reflux.connect(AppStore)],

  getDefaultProps: function() {
    return {
      color: "color-1"
    }
  },

  getInitialState() {
    return({ view: "List", color: this.props.color })
  },

  componentWillMount() {
    AppActions.setDefaults({ addonId: this.props.addonId })
  },

  view() {
    switch(this.state.view) {
      case "List": return <List color={this.state.color} />
      case "Settings": return <Settings />
      default: return <List />
    }
  },

  render() {
    return( this.view())
  }
});

module.exports = App;
