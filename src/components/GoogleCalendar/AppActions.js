var AppActions = Reflux.createActions([
  "changeColor",
  "changeView",
  "setDefaults"
])

module.exports = AppActions;
