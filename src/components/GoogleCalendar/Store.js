var Actions = require("./Actions");

var Store = Reflux.createStore({
  listenables: [Actions],

  load() {
    var ItemsCollection = Backbone.Collection.extend({
      url: window.location.pathname + "/google_calendar/items"
    });

    var collection = new ItemsCollection();

    collection.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  fetch() {
    this.list.fetch({
      success: (collection, response, options) => {
        this.list = collection;
        this.updateList();
      }
    });
  },

  updateList() {
    this.trigger(this.list);
  }
});

module.exports = Store;
