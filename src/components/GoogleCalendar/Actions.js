var Actions = Reflux.createActions([
  "load",
  "fetch"
]);

module.exports = Actions;
