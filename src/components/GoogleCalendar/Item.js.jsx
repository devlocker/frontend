var moment = require("moment")
var Actions = require("./Actions");

var Item = React.createClass({
  getInitialState() {
    return {
      summary: this.props.model.get("summary"),
      start: this.props.model.get("start"),
      html_link: this.props.model.get("html_link")
    };
  },

  _startTime() {
    if(this.state.start === null) {
      return("All Day")
    } else {
      return(moment(this.state.start).format("h:mm a"))
    }
  },

  render() {
    return(
      <div className="list-event" key={this.props.model.id}>
        <span>
        <a href={this.state.html_link} target="_blank"> {this.state.summary}</a>
        </span>
        <p className="list-content">{this._startTime()}</p>
      </div>
    );
  }
});

module.exports = Item;
