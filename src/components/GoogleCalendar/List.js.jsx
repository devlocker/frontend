var Actions = require("./Actions");
var AppActions = require("./AppActions");
var Store = require("./Store");
var Item = require("./Item.js.jsx");
var SetIntervalMixin = require("../../mixins/SetIntervalMixin");

var List = React.createClass({
  mixins: [Reflux.connect(Store, "list"), SetIntervalMixin],

  getInitialState() {
    return { list: [] }
  },

  componentDidMount() {
    Actions.load();
  },

  changeToSettingsView() {
    AppActions.changeView("Settings");
  },

  render() {
    return(
      <div className={`widget ${this.props.color}`}>
        <header>
          <h1>Google Calendar</h1>
        </header>

        <article className="basic-list">
          <section className="list-header">
            <p>Today - {new Date().toDateString()}</p>
          </section>
          <section>
            {this.state.list.map(
              item => {
                return(<Item key={item.id} model={item} />)
              })
            }
          </section>
        </article>

        <footer>
          <a href="#">Refresh</a> / <a href="#" onClick={this.changeToSettingsView}>Settings</a>
        </footer>
      </div>
    );
  }
});

module.exports = List;
