import React, { Component, PropTypes } from 'react';

export default class Footer extends Component {
  render() {
    const styles = require('./Footer.scss');
    return (
      <div className='container'>
        <footer className={styles.mainFooter}>
          <ul>
            <p>DevLocker</p>
            <li><a href='https://github.com/devlocker/devlocker'>Source Code</a></li>
            <li><a href='#'>Install</a></li>
            <li><a href='#'>Mobile</a></li>
            <li><a href='#'>Tour</a></li>
          </ul>
          <ul>
            <p>About Us</p>
            <li><a href='http://devlocker.github.io'>Blog</a></li>
            <li><a href='http://devlocker.github.io'>Team</a></li>
            <li><a href='http://devlocker.github.io'>News</a></li>
            <li><a href='http://devlocker.github.io'>Jobs</a></li>
          </ul>
          <ul>
            <p>Support</p>
            <li><a href='https://github.com/devlocker/devlocker'>Help</a></li>
            <li><a href='https://github.com/devlocker/devlocker'>Documentation</a></li>
            <li><a href='#'>Talk to a Person</a></li>
            <li><a href='#'>Privacy</a></li>
          </ul>
          <ul>
            <p>Community</p>
            <li><a href='https://github.com/orgs/devlocker/teams/owners'>Github</a></li>
            <li><a href='#'>Twitter</a></li>
            <li><a href='https://github.com/orgs/devlocker/teams/owners'>Developers</a></li>
          </ul>
        </footer>
      </div>
    );
  }
}
