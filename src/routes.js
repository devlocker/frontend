import React from 'react';
import {Route} from 'react-router';
import {
    App,
    Home,
    Addons,
    Widgets,
    About,
    Login,
    RequireLogin,
    LoginSuccess,
    Survey,
    NotFound,
  } from 'containers';

export default function(store) {
  return (
    <Route component={App}>
      <Route path="/login" component={Login}/>
      <Route component={RequireLogin} onEnter={RequireLogin.onEnter(store)}>
        <Route path="/" component={Home}/>
        <Route path="/addons" component={Addons}/>
        <Route path="/widgets" component={Widgets}/>
        <Route path="/about" component={About}/>
        <Route path="/loginSuccess" component={LoginSuccess}/>
        <Route path="/survey" component={Survey}/>
      </Route>
      <Route path="*" component={NotFound}/>
    </Route>
  );
}
