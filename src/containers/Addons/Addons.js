import React, { Component } from 'react';
import configureStore from './stores/configure';
import { Provider } from 'react-redux';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import EmptyAddon from './components/EmptyAddon';
import { Basecamp, Bugsnag } from './containers';
import TodoApp from './components/Todo/App.js.jsx';
import JiraApp from './components/Jira/JiraList.js.jsx';
import GoogleCalendarApp from './components/GoogleCalendar/App.js.jsx';
import GithubApp from './components/Github/App.js.jsx';

const store = configureStore();

export default class Root extends Component {
  _renderDevTools() {
    if (__DEV__) {
      return(
        <DebugPanel top right bottom>
          <DevTools store={store} monitor={LogMonitor} />
        </DebugPanel>
      );
    }
  }

  _availableAddons() {
    return {
      BasecampApp: BasecampApp,
      BugsnagApp: BugsnagApp,
      TodosApp: TodoApp,
      GoogleApp: GoogleCalendarApp,
      JiraApp: JiraApp,
      GithubApp: GithubApp
    };
  }

  _enabledAddons() {
    return(
      this.props.enabledAddons.map(
        (addon) => {
          return(
            this._renderAddon(this._availableAddons()[addon])
          );
        }
      )
    );
  }

  _renderAddon(addon) {
    return(
      React.createElement(addon)
    );
  }

  _left() {
    return(
      this._enabledAddons().splice(0, this._enabledAddons().length / 2)
    );
  }

  _right() {
    return(
      this._enabledAddons().splice(this._enabledAddons().length / 2, this._enabledAddons().length)
    );
  }

  _addons(){
    if (this.props.enabledAddons.length === 0) {
      return(
        <div className='grid'>
          <div className='big-column'>
            {this._renderAddon(EmptyAddon)}
          </div>
        </div>
      )
    } else {
      return(
        <div className='grid'>
          <div className='big-column'>
            {this._left()}
          </div>
          <div className='big-column'>
            {this._right()}
          </div>
        </div>
      );
    }
  }

  render() {
    return(
      <div>
        <Provider store={store}>
        {
          () => {
            return(
              <div>
                {this._addons()}
              </div>
            );
          }
        }
        </Provider>
        {this._renderDevTools()}
      </div>
    );
  }
}
