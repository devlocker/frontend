import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { MainSection } from 'components';
import { loadLists, loadTodos, completeTodo } from 'redux/modules/basecamp';

function mapStateToProps(state) {
  return {
    lists: state.basecamp.get('lists'),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadLists: () => dispatch(loadLists()),
    loadTodos: (listId) => dispatch(loadTodos(listId)),
    completeTodo: (listId, todoId) => dispatch(completeTodo(listId, todoId)),
  };
}

@connect(mapStateToProps, mapDispatchToProps)
export default class BasecampApp extends Component {
  static propTypes = {
    lists: PropTypes.array.isRequired,
    loadLists: PropTypes.func.isRequired,
    loadTodos: PropTypes.func.isRequired,
    completeTodo: PropTypes.func.isRequired,
  }

  render() {
    return(
      <MainSection
        lists={this.props.lists}
        loadLists={this.props.loadLists}
        loadTodos={this.props.loadTodos}
        completeTodo={this.props.completeTodo}
      />
    );
  }
}
