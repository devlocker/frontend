import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { MostCommon, MostRecent} from 'components';
import { loadCommonErrors, loadRecentErrors } from 'redux/modules/bugsnag';

function mapStateToProps(state) {
  return {
    recent: state.bugsnag.get('recent'),
    common: state.bugsnag.get('common'),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadCommonErrors: () => dispatch(loadCommonErrors()),
    loadRecentErrors: () => dispatch(loadRecentErrors()),
  };
};

@connect(mapStateToProps, mapDispatchToProps)
export default class Bugsnag extends Component {
  static propTypes = {
    recent: PropTypes.array.isRequired,
    common: PropTypes.array.isRequired,
    loadCommonErrors: PropTypes.func.isRequired,
    loadRecentErrors: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.loadCommonErrors();
    this.props.loadRecentErrors();
  }

  render() {
    return(
      <article className='widget color-1'>
        <header>
          <h1>Bugsnag</h1>
        </header>
        <article>
          <MostCommon
            errors={this.props.common}
          />
          <MostRecent
            errors={this.props.recent}
          />
        </article>

        <footer>
          <a href='#'>Refresh</a> / <a href='#'>Settings</a>
        </footer>
      </article>
    );
  }
}
