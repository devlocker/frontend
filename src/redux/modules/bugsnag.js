import axios from 'axios';
import { resolve } from 'redux-simple-promise';
import { fromJS } from 'immutable';

const LOAD_COMMON_ERRORS = 'LOAD_COMMON_ERRORS';
const LOAD_RECENT_ERRORS = 'LOAD_RECENT_ERRORS';

export const initialState = fromJS({
  common: {},
  recent: {}
});

export default function bugsnag(state = initialState, action) {
  switch (action.type) {
  case resolve(LOAD_COMMON_ERRORS):
    return state.setIn(['common'], fromJS(action.payload.data));
  case resolve(LOAD_RECENT_ERRORS):
    return state.setIn(['recent'], fromJS(action.payload.data));
  default:
    return state;
  }
}

export function loadCommonErrors() {
  const url = `${window.location.pathname}/bugsnag/errors/common.json`;

  return {
    type: types.LOAD_COMMON_ERRORS,
    payload: {
      promise: axios.get(url)
    }
  };
}

export function loadRecentErrors() {
  const url = `${window.location.pathname}/bugsnag/errors/recent.json`;

  return {
    type: types.LOAD_RECENT_ERRORS,
    payload: {
      promise: axios.get(url)
    }
  };
}
