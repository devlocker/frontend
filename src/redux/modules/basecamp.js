import axios from 'axios';
import { resolve, reject } from 'redux-simple-promise';
import { Map, fromJS } from 'immutable';

const LOAD_LISTS = 'LOAD_LISTS';
const LOAD_TODOS = 'LOAD_TODOS';
const COMPLETE_TODO = 'COMPLETE_TODO';

const initialState = Map({
  lists: Map({})
});

export default function basecamp(state = initialState, action) {
  switch (action.type) {
  case resolve(LOAD_LISTS):
    return state.setIn(['lists'], fromJS(action.payload.data));
  case resolve(LOAD_TODOS):
    return(
      state.
        setIn(
          ['lists', action.meta.listId.toString(), 'todos'],
          fromJS(action.payload.data)
        )
    );
  case resolve(COMPLETE_TODO):
    return(
      state.
        setIn(
          ['lists', action.meta.listId.toString(), 'todos', action.meta.todoId.toString(), 'completed'],
          true
        )
    );
  default:
    return state;
  }
}

export function loadLists() {
  const url = `${window.location.pathname}/basecamp/todolists.json`;

  return {
    type: types.LOAD_LISTS,
    payload: {
      promise: axios.get(url)
    }
  };
}

export function loadTodos(listId) {
  const url = `${window.location.pathname}/basecamp/todos.json`;

  return {
    type: types.LOAD_TODOS,
    payload: {
      promise: axios.get(url, { params: { todolist_id: listId } }),
      listId: listId,
    }
  };
}

export function completeTodo(listId, todoId) {
  const url = `${window.location.pathname}/basecamp/todos/${todoId}/complete.json`;

  return {
    type: types.COMPLETE_TODO,
    payload: {
      promise: axios.post(url, {}, { headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') } }),
      listId: listId,
      todoId: todoId,
    }
  };
}
