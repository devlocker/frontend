var React = require("react");
var Reflux = require("reflux");
var _ = require("underscore");
var Backbone = require("backbone");

window._ = _;
window.Backbone = Backbone;
window.React = React;
window.Reflux = Reflux;

var Devlocker = {
  Root: require("./root.js"),
  SignUpTerminal: {
    Terminal: require("./components/SignUpTerminal/SignUpTerminal.js.jsx")
  }
};

module.exports = Devlocker;

window.Devlocker = Devlocker;

Backbone.$ = $;
