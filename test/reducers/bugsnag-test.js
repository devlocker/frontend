import { assert } from 'chai';
import { fromJS, Map } from 'immutable';
import bugsnag, { initialState } from '../../reducers/bugsnag';

describe('reducer', () => {
  describe('initialState', () => {
    it('returns the initialState', () => {
      const expectedState = fromJS({
        common: {},
        recent: {},
      });
      assert.equal(initialState, expectedState);
    });
  });

  describe('bugsnag', () => {
    describe('LOAD_COMMON_ERRORS_RESOLVED', () => {
      it('returns new state that includes payload data', () => {
        const action = {
          payload: {
            data: [
              {
                id: 1,
                class: 'NameError',
              }
            ]
          },
          type: 'LOAD_COMMON_ERRORS_RESOLVED'
        };
        const expectedState = fromJS({
          common: action.payload.data,
          recent: {},
        });
        assert.equal(bugsnag(initialState, action), expectedState);
      });
    });

    describe('LOAD_RECENT_ERRORS_RESOLVED', () => {
      it('returns new state that includes payload data', () => {
        const action = {
          payload: {
            data: [
              {
                id: 1,
                class: 'NameError',
              }
            ]
          },
          type: 'LOAD_RECENT_ERRORS_RESOLVED'
        };
        const expectedState = fromJS({
          common: {},
          recent: action.payload.data,
        });
        assert.equal(bugsnag(initialState, action), expectedState);
      });
    });

    describe('default', () => {
      it('returns the passed in state', () => {
        const action = {
          payload: {},
          type: 'UNSUPPORTED_ACTION'
        };
        const expectedState = fromJS({
          common: { id: 1 },
          recent: { id: 2 }
        });
        assert.equal(bugsnag(expectedState, action), expectedState);
      });
    });
  });
});
